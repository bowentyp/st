# st_stm32l4r9i_disco

- [介绍](#1)
- [使用说明](#2)
- [相关文件简介](#3)
- [参与贡献](#4)

#### 介绍 <a name = "1"></a>
该仓包含minidisplay demo的stm32l4r9i驱动源码、lcd示例代码，demo依赖liteos_m内核，可以进行任务调度、lcd显示。本仓一般被放置在device目录，详细开发指导文档见[ohos_platform_docs/stm32l4r9](https://gitee.com/haizhouyang/ohos_platform_docs/tree/master/soc/stm32l4r9/l0)。

#### 使用说明 <a name = "2"></a>

1.  获取[OpenHarmony](https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/get-code/%E6%BA%90%E7%A0%81%E8%8E%B7%E5%8F%96.md)完整仓代码。
2.  参考[快速入门](https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/quick-start/Readme-CN.md)搭建环境。
3.  cd进入vendor/huawei目录，git clone https://gitee.com/harylee/minidisplay_demo.git 。
4.  cd进入device目录，git clone https://gitee.com/harylee/st.git 。
5.  下载arm交叉编译器，git clone https://gitee.com/harylee/gcc-arm-none-eabi-10-2020-q4-major.git ，将交叉编译器环境变量bin目录配置到.bashrc文件中或者配置device/st/stm32l4r9i_disco/liteos_m/config.gni文件中board_toolchain_path宏为交叉编译器bin路径。
6.  在OpenHarmony根目录：

```shell
haryslee@dev:~/OpenHarmony$ hb set
[OHOS INFO] Input code path:
OHOS Which product do you need?  minidisplay_demo@huawei
haryslee@dev:~/OpenHarmony$ hb build
```

7.  最终的镜像生成在out/stm32l4r9i_disco/minidisplay_demo/目录中，通过STM32 ST-LINK Utility软件将DISCO_L4R9I.hex下载至单板即可。

```shell
haryslee@dev:~/OpenHarmony$ ll out/stm32l4r9i_disco/minidisplay_demo/
total 3836
drwxr-xr-x 1 haryslee haryslee     512 Apr 13 15:38 ./
drwxr-xr-x 1 haryslee haryslee     512 Apr 13 15:38 ../
-rw-r--r-- 1 haryslee haryslee     300 Apr 13 15:38 .ninja_deps
-rw-r--r-- 1 haryslee haryslee   15740 Apr 13 15:38 .ninja_log
-rwxr-xr-x 1 haryslee haryslee  522952 Apr 13 15:38 DISCO_L4R9I.bin*
-rwxr-xr-x 1 haryslee haryslee  635092 Apr 13 15:38 DISCO_L4R9I.elf*
-rw-r--r-- 1 haryslee haryslee 1470992 Apr 13 15:38 DISCO_L4R9I.hex
-rw-r--r-- 1 haryslee haryslee 1182643 Apr 13 15:38 DISCO_L4R9I.map
-rw-r--r-- 1 haryslee haryslee     365 Apr 13 15:38 args.gn
-rw-r--r-- 1 haryslee haryslee   65773 Apr 13 15:38 build.log
-rw-r--r-- 1 haryslee haryslee    4617 Apr 13 15:38 build.ninja
-rw-r--r-- 1 haryslee haryslee    1572 Apr 13 15:38 build.ninja.d
drwxr-xr-x 1 haryslee haryslee     512 Apr 13 15:38 gen/
drwxr-xr-x 1 haryslee haryslee     512 Apr 13 15:38 libs/
drwx------ 1 haryslee haryslee     512 Apr 13 15:38 obj/
-rw-r--r-- 1 haryslee haryslee    4600 Apr 13 15:38 toolchain.ninja
```

```
注：
1、确保usb stlink连接线无问题；
2、若镜像烧录后系统无响应，请重新插拔stlink供电线。
```


#### 相关文件简介 <a name = "3"></a>

```shell
/stm32l4r9i_disco                
├── Core          
│   ├── Inc
│   │   ├── main.h
│   │   ├── platform_test.h
│   │   └── ……                 # 其他头文件不用改，省略
│   └── Src
│   │   ├── main.c             # 有一个printf重定向write函数
│   │   ├── platform_test.c    # spi测试文件
│   │   ├── task_sample.c      # 依赖liteos内核的任务调度，包括HDF初始化、i2c和uart测试任务
│   │   └── ……                 # 其他头文件不用改，省略
│   └── BUILD.gn               # 针对Core的编译文件，可以根据测试需求取消对应的注释 
├── Drivers
│   ├── BSP                    # stm32L4R9-DISCO开发板官方驱动 
│   ├── CMSIS                  # cortex-M的相关文件 
│   ├── hdf                    # HDF驱动文件 
│   │   ├── uart_stm32l4xx.c
│   │   ├── i2c_stm32l4xx.c
│   │   ├── spi_stm32l4xx.c
│   │   ├── gpio_stm32l4xx.c  
│   │   ├── BUILD.gn           # 针对hdf的编译文件
│   │   └── ……
│   └── STM32L4xx_HAL_Driver   # STM32L4的HAL库函数 
├── liteos_m
│   └── config.gni             # 是适配liteos_m的配置文件
├── Utilities
│   └── Fonts                  # stm32L4R9-DISCO开发板官方驱动自带字体
├── BUILD.gn                   # 几乎不用改
├── startup_stm32l4r9xx.s      # 启动文件
├── STM32L4R9AIIx_FLASH.ld     # 启动文件
├── Makefile                   # 几乎不用改，出现链接问题可以看看STATIC_LIB那里
└── target_config.h            # liteos_m内核的宏定义文件，包括时钟、是否接受中断、任务管理、信号量、互斥锁、内存管理等
```

- 说明

**BSP**和**Utilities**两个文件夹来自于[stm32L4R9-DISCO开发板官方驱动+例程](https://github.com/STMicroelectronics/STM32CubeL4)。


#### 参与贡献 <a name = "4"></a>

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

