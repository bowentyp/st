#ifndef GPIO_HI35XX_H
#define GPIO_HI35XX_H

#include "../STM32L4xx_HAL_Driver/Inc/stm32l4xx_hal.h"
#include "securec.h"
#include "device_resource_if.h"
#include "hdf_device_desc.h"
#include "hdf_log.h"
#include "gpio_core.h" 
#include "osal_mem.h"
#include "osal_spinlock.h" 
#include "../CMSIS/Device/ST/STM32L4xx/Include/stm32l4r9xx.h"
#include "../STM32L4xx_HAL_Driver/Inc/stm32l4xx_hal_gpio.h"
#include "gpio_core.h"
#include "osal_irq.h"

#define STM32L4_GROUP_MAX 9
#define STM32L4_BIT_MAX   16 
#define STM32L4_PIN_MAX   140 
// #define STM32L4XX_I2C_TIMEOUT 0xFFFF // ticks
struct Stm32L4xxGpioGroup {
    uint32_t pinDir;
    uint32_t pinInitFlag;
    uint32_t ITFlag;
    OsalSpinlock lock;
};

struct Stm32L4xxGpioCntlr {
    struct GpioCntlr cntlr;
    GPIO_InitTypeDef *GPIO_InitStruct;
    uint8_t groupNum;
    uint8_t bitNum;
    uint16_t pinCount;
    struct Stm32L4xxGpioGroup *groups;

};


#endif /* GPIO_HI35XX_H */