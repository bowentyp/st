#ifndef I2C_HI35XX_H
#define I2C_HI35XX_H

#include "../STM32L4xx_HAL_Driver/Inc/stm32l4xx_hal.h"
#include "securec.h"
#include "device_resource_if.h"
#include "hdf_device_desc.h"
#include "hdf_log.h"
#include "i2c_core.h" 
#include "osal_mem.h"
#include "osal_spinlock.h" 
#include "../CMSIS/Device/ST/STM32L4xx/Include/stm32l4r9xx.h"
#include "../STM32L4xx_HAL_Driver/Inc/stm32l4xx_hal_i2c.h"

#define STM32L4XX_I2C_TIMEOUT 0xFFFF // ticks
struct Stm32xxI2cCntlr {
    struct I2cCntlr cntlr;
    I2C_HandleTypeDef *i2cHandle;
    OsalSpinlock spin; 
    int16_t bus;
    uint32_t clk;
    uint32_t freq;
    uint32_t irq;
};

#endif /* I2C_HI35XX_H */