/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "../Inc/main.h"
#include <stdio.h>
/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
SPI_HandleTypeDef hspi2;

UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_SPI2_Init(void);
static void MX_USART2_UART_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
int _write(int fd, char *buffer, int size)
{
	HAL_UART_Transmit(&huart2, (uint8_t *)buffer, size, 0xFFFF);
	return size;
}

// void HAL_Delay(uint32_t Delay)
// {
//   for(uint32_t cnt = 0xFFFFF * Delay; cnt > 1; --cnt);
// }

FlagStatus LedInitialized = RESET;
FlagStatus JoyInitialized = RESET;

void SystemHardwareInit(void)
{
  /* Init LED 1 to 4  */
  if(LedInitialized != SET)
  {
    BSP_LED_Init(LED1);
    BSP_LED_Init(LED2);
    LedInitialized = SET;
  }

  /* Init Joystick in interrupt mode */
  if(JoyInitialized != SET)
  {
    BSP_JOY_Init(JOY_MODE_EXTI);
    JoyInitialized = SET;
  }

  /* For external component access over I2C */
  __HAL_RCC_I2C1_CLK_ENABLE();
}
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */
  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  // MX_GPIO_Init();
  // MX_SPI2_Init();
  MX_USART2_UART_Init();
  /* USER CODE BEGIN 2 */

  // SystemHardwareInit();
  
  // BSP_IO_ConfigPin(IO_PIN_6 | IO_PIN_7, IO_MODE_OUTPUT);
  // BSP_IO_WritePin(IO_PIN_6 | IO_PIN_7, GPIO_PIN_RESET);
  
  /*要使用LED1的话只能采用这些函数：BSP_LED_On(LED1) BSP_LED_Off BSP_LED_Toggle*/

  // HAL_Delay(500);
  
  HDF_Init();
  // SPI_Flash_Test();
  // GPIO_NormalTest();
  GPIO_ISRTest();

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */
    /* USER CODE BEGIN 3 */

     for(uint32_t cnt = 0x000FFFFF; cnt >= 1; --cnt);
    // HAL_Delay(300);
    printf("hello \n");
    // HAL_GPIO_TogglePin(GPIOB,GPIO_PIN_13 | GPIO_PIN_14 | GPIO_PIN_15);//测试PB13、14、15的可行性
    // HAL_GPIO_TogglePin(LED_GREEN_GPIO_Port,LED_GREEN_Pin);
    // BSP_LED_Toggle(LED_ORANGE);
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1_BOOST) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_MSI;
  RCC_OscInitStruct.MSIState = RCC_MSI_ON;
  RCC_OscInitStruct.MSICalibrationValue = 0;
  RCC_OscInitStruct.MSIClockRange = RCC_MSIRANGE_6;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_MSI;
  RCC_OscInitStruct.PLL.PLLM = 1;
  RCC_OscInitStruct.PLL.PLLN = 60;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV5;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief SPI2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI2_Init(void)
{

  /* USER CODE BEGIN SPI2_Init 0 */

  /* USER CODE END SPI2_Init 0 */

  /* USER CODE BEGIN SPI2_Init 1 */

  /* USER CODE END SPI2_Init 1 */
  /* SPI2 parameter configuration*/
  hspi2.Instance = SPI2;
  hspi2.Init.Mode = SPI_MODE_MASTER;
  hspi2.Init.Direction = SPI_DIRECTION_2LINES;
  hspi2.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi2.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi2.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi2.Init.NSS = SPI_NSS_SOFT;
  hspi2.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_256;
  hspi2.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi2.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi2.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi2.Init.CRCPolynomial = 7;
  hspi2.Init.CRCLength = SPI_CRC_LENGTH_DATASIZE;
  hspi2.Init.NSSPMode = SPI_NSS_PULSE_DISABLE;
  if (HAL_SPI_Init(&hspi2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI2_Init 2 */

  /* USER CODE END SPI2_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.Init.ClockPrescaler = UART_PRESCALER_DIV1;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_SetTxFifoThreshold(&huart2, UART_TXFIFO_THRESHOLD_1_8) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_SetRxFifoThreshold(&huart2, UART_RXFIFO_THRESHOLD_1_8) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_DisableFifoMode(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  // __HAL_RCC_GPIOA_CLK_ENABLE();
  // __HAL_RCC_GPIOB_CLK_ENABLE();
  // __HAL_RCC_GPIOC_CLK_ENABLE();
  // __HAL_RCC_GPIOD_CLK_ENABLE();
  // __HAL_RCC_GPIOE_CLK_ENABLE();
  // __HAL_RCC_GPIOF_CLK_ENABLE();
  // __HAL_RCC_GPIOG_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  // __HAL_RCC_GPIOI_CLK_ENABLE();
  // HAL_PWREx_EnableVddIO2();

  /*Configure GPIO pin Output Level */
  // HAL_GPIO_WritePin(GPIOH, MIC_VDD_Pin|DSI_SPI_USART_CS_Pin|LED_GREEN_Pin, GPIO_PIN_RESET);

  // /*Configure GPIO pin Output Level */
  // HAL_GPIO_WritePin(JOY_SEL_GPIO_Port, JOY_SEL_Pin, GPIO_PIN_RESET);
  // HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0, GPIO_PIN_RESET);

  // /*Configure GPIO pin Output Level */
  // // HAL_GPIO_WritePin(SPI2_CS_GPIO_Port, SPI2_CS_Pin | GPIO_PIN_7, GPIO_PIN_RESET);

  // /*Configure GPIO pin Output Level */
  // HAL_GPIO_WritePin(MFX_WAKEUP_GPIO_Port, MFX_WAKEUP_Pin, GPIO_PIN_RESET);

  // /*Configure GPIO pins : OCTOSPIM_P2_IO1_Pin OCTOSPIM_P2_IO2_Pin OCTOSPIM_P2_CLK_Pin OCTOSPIM_P2_IO0_Pin */
  // GPIO_InitStruct.Pin = OCTOSPIM_P2_IO1_Pin|OCTOSPIM_P2_IO2_Pin|OCTOSPIM_P2_CLK_Pin|OCTOSPIM_P2_IO0_Pin;
  // GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  // GPIO_InitStruct.Pull = GPIO_NOPULL;
  // GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  // GPIO_InitStruct.Alternate = GPIO_AF5_OCTOSPIM_P2;
  // HAL_GPIO_Init(GPIOI, &GPIO_InitStruct);

  /*Configure GPIO pins : MIC_VDD_Pin DSI_SPI_USART_CS_Pin LED_GREEN_Pin */
  GPIO_InitStruct.Pin = LED_GREEN_Pin;//MIC_VDD_Pin|DSI_SPI_USART_CS_Pin|
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  HAL_GPIO_Init(GPIOH, &GPIO_InitStruct);

  /*Configure GPIO pins : PSRAM_NBL0_Pin PSRAM_NBL1_Pin PSRAM_A20_Pin PSRAM_A19_Pin
                           D7_Pin D6_Pin D12_Pin D5_Pin
                           D11_Pin D4_Pin D10_Pin D9_Pin
                           D8_Pin */
  // GPIO_InitStruct.Pin = PSRAM_NBL0_Pin|PSRAM_NBL1_Pin|PSRAM_A20_Pin|PSRAM_A19_Pin
  //                         |D7_Pin|D6_Pin|D12_Pin|D5_Pin
  //                         |D11_Pin|D4_Pin|D10_Pin|D9_Pin
  //                         |D8_Pin;
  // GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  // GPIO_InitStruct.Pull = GPIO_NOPULL;
  // GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  // GPIO_InitStruct.Alternate = GPIO_AF12_FMC;
  // HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  // /*Configure GPIO pin : ARD_D6_Pin */
  // GPIO_InitStruct.Pin = ARD_D6_Pin;
  // GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  // GPIO_InitStruct.Pull = GPIO_NOPULL;
  // GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  // GPIO_InitStruct.Alternate = GPIO_AF2_TIM3;
  // HAL_GPIO_Init(ARD_D6_GPIO_Port, &GPIO_InitStruct);

  // /*Configure GPIO pins : DCMI_D7_Pin DCMI_D5_Pin DCMI_VSYNC_Pin */
  // GPIO_InitStruct.Pin = DCMI_D7_Pin|DCMI_D5_Pin|DCMI_VSYNC_Pin;
  // GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  // GPIO_InitStruct.Pull = GPIO_NOPULL;
  // GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  // GPIO_InitStruct.Alternate = GPIO_AF10_DCMI;
  // HAL_GPIO_Init(GPIOI, &GPIO_InitStruct);

  // /*Configure GPIO pins : SAI1_SDB_Pin SAI1_FSA_Pin */
  // GPIO_InitStruct.Pin = SAI1_SDB_Pin|SAI1_FSA_Pin;
  // GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  // GPIO_InitStruct.Pull = GPIO_NOPULL;
  // GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  // GPIO_InitStruct.Alternate = GPIO_AF13_SAI1;
  // HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  // /*Configure GPIO pins : OCTOSPIM_P2_IO6_Pin OCTOSPIM_P2_DQS_Pin OCTOSPIM_P2_IO7_Pin OCTOSPIM_P2_CS_Pin */
  // GPIO_InitStruct.Pin = OCTOSPIM_P2_IO6_Pin|OCTOSPIM_P2_DQS_Pin|OCTOSPIM_P2_IO7_Pin|OCTOSPIM_P2_CS_Pin;
  // GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  // GPIO_InitStruct.Pull = GPIO_NOPULL;
  // GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  // GPIO_InitStruct.Alternate = GPIO_AF5_OCTOSPIM_P2;
  // HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);

  // /*Configure GPIO pins : D2_Pin PSRAM_OE_Pin D3_Pin PSRAM_WE_Pin
  //                          PSRAM_WAIT_Pin PSRAM_CLK_Pin PSRAM_NE1_Pin PSRAM_A18_Pin
  //                          D1_Pin D0_Pin PSRAM_A17_Pin PSRAM_A16_Pin
  //                          D15_Pin D14_Pin D13_Pin */
  // GPIO_InitStruct.Pin = D2_Pin|PSRAM_OE_Pin|D3_Pin|PSRAM_WE_Pin
  //                         |PSRAM_WAIT_Pin|PSRAM_CLK_Pin|PSRAM_NE1_Pin|PSRAM_A18_Pin
  //                         |D1_Pin|D0_Pin|PSRAM_A17_Pin|PSRAM_A16_Pin
  //                         |D15_Pin|D14_Pin|D13_Pin;
  // GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  // GPIO_InitStruct.Pull = GPIO_NOPULL;
  // GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  // GPIO_InitStruct.Alternate = GPIO_AF12_FMC;
  // HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  // /*Configure GPIO pin : MFX_IRQ_OUT_Pin */
  // GPIO_InitStruct.Pin = MFX_IRQ_OUT_Pin;
  // GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  // GPIO_InitStruct.Pull = GPIO_NOPULL;
  // HAL_GPIO_Init(MFX_IRQ_OUT_GPIO_Port, &GPIO_InitStruct);

  // /*Configure GPIO pins : DCMI_D3_Pin DCMI_PIXCLK_Pin DCMI_D2_Pin */
  // GPIO_InitStruct.Pin = DCMI_D3_Pin|DCMI_PIXCLK_Pin|DCMI_D2_Pin;
  // GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  // GPIO_InitStruct.Pull = GPIO_NOPULL;
  // GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  // GPIO_InitStruct.Alternate = GPIO_AF10_DCMI;
  // HAL_GPIO_Init(GPIOH, &GPIO_InitStruct);

  // /*Configure GPIO pin : I2C1_SCL_Pin */
  // GPIO_InitStruct.Pin = I2C1_SCL_Pin;
  // GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
  // GPIO_InitStruct.Pull = GPIO_PULLUP;
  // GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  // GPIO_InitStruct.Alternate = GPIO_AF4_I2C1;
  // HAL_GPIO_Init(I2C1_SCL_GPIO_Port, &GPIO_InitStruct);

  // /*Configure GPIO pin : ARD_D9_Pin */
  // GPIO_InitStruct.Pin = ARD_D9_Pin;
  // GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  // GPIO_InitStruct.Pull = GPIO_NOPULL;
  // GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  // GPIO_InitStruct.Alternate = GPIO_AF3_TIM8;
  // HAL_GPIO_Init(ARD_D9_GPIO_Port, &GPIO_InitStruct);

  // /*Configure GPIO pins : OCTOPSIM_P2_IO4_Pin OCTOSPIM_P2_IO5_Pin OCTOSPI_P2_IO3_Pin */
  // GPIO_InitStruct.Pin = OCTOPSIM_P2_IO4_Pin|OCTOSPIM_P2_IO5_Pin|OCTOSPI_P2_IO3_Pin;
  // GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  // GPIO_InitStruct.Pull = GPIO_NOPULL;
  // GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  // GPIO_InitStruct.Alternate = GPIO_AF5_OCTOSPIM_P2;
  // HAL_GPIO_Init(GPIOH, &GPIO_InitStruct);

  // /*Configure GPIO pins : SAI1_MCKA_Pin SAI1_SDA_Pin SAI1_SCKA_Pin */
  // GPIO_InitStruct.Pin = SAI1_MCKA_Pin|SAI1_SDA_Pin|SAI1_SCKA_Pin;
  // GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  // GPIO_InitStruct.Pull = GPIO_NOPULL;
  // GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  // GPIO_InitStruct.Alternate = GPIO_AF13_SAI1;
  // HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  // /*Configure GPIO pin : PSRAM_ADV_Pin */
  // GPIO_InitStruct.Pin = PSRAM_ADV_Pin;
  // GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  // GPIO_InitStruct.Pull = GPIO_NOPULL;
  // GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  // GPIO_InitStruct.Alternate = GPIO_AF12_FMC;
  // HAL_GPIO_Init(PSRAM_ADV_GPIO_Port, &GPIO_InitStruct);

  // /*Configure GPIO pin : uSD_CMD_Pin */
  // GPIO_InitStruct.Pin = uSD_CMD_Pin;
  // GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  // GPIO_InitStruct.Pull = GPIO_NOPULL;
  // GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  // GPIO_InitStruct.Alternate = GPIO_AF12_SDMMC1;
  // HAL_GPIO_Init(uSD_CMD_GPIO_Port, &GPIO_InitStruct);

  // /*Configure GPIO pins : uSD_D2_Pin uSD_D3_Pin uSD_CLK_Pin uSD_D0_Pin
  //                          uSD_D1_Pin */
  // GPIO_InitStruct.Pin = uSD_D2_Pin|uSD_D3_Pin|uSD_CLK_Pin|uSD_D0_Pin
  //                         |uSD_D1_Pin;
  // GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  // GPIO_InitStruct.Pull = GPIO_NOPULL;
  // GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  // GPIO_InitStruct.Alternate = GPIO_AF12_SDMMC1;
  // HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  // /*Configure GPIO pins : USB_OTGFS_ID_Pin USB_OTG_FS_DP_Pin USB_OTGFS_DM_Pin */
  // GPIO_InitStruct.Pin = USB_OTGFS_ID_Pin|USB_OTG_FS_DP_Pin|USB_OTGFS_DM_Pin;
  // GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  // GPIO_InitStruct.Pull = GPIO_NOPULL;
  // GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  // GPIO_InitStruct.Alternate = GPIO_AF10_OTG_FS;
  // HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  // /*Configure GPIO pin : JOY_SEL_Pin */
  // GPIO_InitStruct.Pin = JOY_SEL_Pin;
  // GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  // GPIO_InitStruct.Pull = GPIO_NOPULL;
  // GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  // HAL_GPIO_Init(JOY_SEL_GPIO_Port, &GPIO_InitStruct);

  // /*Configure GPIO pin : USB_OTGFS_VBUS_Pin */
  // GPIO_InitStruct.Pin = USB_OTGFS_VBUS_Pin;
  // GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  // GPIO_InitStruct.Pull = GPIO_NOPULL;
  // HAL_GPIO_Init(USB_OTGFS_VBUS_GPIO_Port, &GPIO_InitStruct);

  // /*Configure GPIO pins : PSRAM_A2_Pin PSRAM_A1_Pin PSRAM_A0_Pin PSRAM_A3_Pin
  //                          PSRAM_A4_Pin PSRAM_A5_Pin PSRAM_A9_Pin PSRAM_A8_Pin
  //                          PSRAM_A7_Pin PSRAM_A6_Pin */
  // GPIO_InitStruct.Pin = PSRAM_A2_Pin|PSRAM_A1_Pin|PSRAM_A0_Pin|PSRAM_A3_Pin
  //                         |PSRAM_A4_Pin|PSRAM_A5_Pin|PSRAM_A9_Pin|PSRAM_A8_Pin
  //                         |PSRAM_A7_Pin|PSRAM_A6_Pin;
  // GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  // GPIO_InitStruct.Pull = GPIO_NOPULL;
  // GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  // GPIO_InitStruct.Alternate = GPIO_AF12_FMC;
  // HAL_GPIO_Init(GPIOF, &GPIO_InitStruct);

  // /*Configure GPIO pins : ARD_14_Pin ARD_15_Pin */
  // GPIO_InitStruct.Pin = ARD_14_Pin|ARD_15_Pin;
  // GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
  // GPIO_InitStruct.Pull = GPIO_NOPULL;
  // GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  // GPIO_InitStruct.Alternate = GPIO_AF4_I2C3;
  // HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);

  // /*Configure GPIO pin : STMOD_INT_Pin */
  // GPIO_InitStruct.Pin = STMOD_INT_Pin;
  // GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  // GPIO_InitStruct.Pull = GPIO_NOPULL;
  // HAL_GPIO_Init(STMOD_INT_GPIO_Port, &GPIO_InitStruct);

  // /*Configure GPIO pin : I2C1_SDA_Pin */
  // GPIO_InitStruct.Pin = I2C1_SDA_Pin;
  // GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
  // GPIO_InitStruct.Pull = GPIO_PULLUP;
  // GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  // GPIO_InitStruct.Alternate = GPIO_AF4_I2C1;
  // HAL_GPIO_Init(I2C1_SDA_GPIO_Port, &GPIO_InitStruct);

  // /*Configure GPIO pins : PSRAM_A14_Pin PSRAM_A13_Pin PSRAM_A15_Pin PSRAM_A11_Pin
  //                          PSRAM_A12_Pin PSRAM_A10_Pin */
  // GPIO_InitStruct.Pin = PSRAM_A14_Pin|PSRAM_A13_Pin|PSRAM_A15_Pin|PSRAM_A11_Pin
  //                         |PSRAM_A12_Pin|PSRAM_A10_Pin;
  // GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  // GPIO_InitStruct.Pull = GPIO_NOPULL;
  // GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  // GPIO_InitStruct.Alternate = GPIO_AF12_FMC;
  // HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);

  // /*Configure GPIO pins : DFDATIN3_Pin DF_CKOUT_Pin */
  // GPIO_InitStruct.Pin = DFDATIN3_Pin|DF_CKOUT_Pin;
  // GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  // GPIO_InitStruct.Pull = GPIO_NOPULL;
  // GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  // GPIO_InitStruct.Alternate = GPIO_AF6_DFSDM1;
  // HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  // /*Configure GPIO pins : ARD_D0_Pin ARD_D1_Pin */
  // GPIO_InitStruct.Pin = ARD_D0_Pin|ARD_D1_Pin;
  // GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  // GPIO_InitStruct.Pull = GPIO_NOPULL;
  // GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  // GPIO_InitStruct.Alternate = GPIO_AF8_LPUART1;
  // HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  // /*Configure GPIO pins : ARD_A2_Pin ARD_A1_Pin */
  // GPIO_InitStruct.Pin = ARD_A2_Pin|ARD_A1_Pin;
  // GPIO_InitStruct.Mode = GPIO_MODE_ANALOG_ADC_CONTROL;
  // GPIO_InitStruct.Pull = GPIO_NOPULL;
  // HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  // /*Configure GPIO pins : ARD_A4_Pin ARD_A0_Pin */
  // GPIO_InitStruct.Pin = ARD_A4_Pin|ARD_A0_Pin;
  // GPIO_InitStruct.Mode = GPIO_MODE_ANALOG_ADC_CONTROL;
  // GPIO_InitStruct.Pull = GPIO_NOPULL;
  // HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  // /*Configure GPIO pin : STMOD_PWM_Pin */
  // GPIO_InitStruct.Pin = STMOD_PWM_Pin;
  // GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  // GPIO_InitStruct.Pull = GPIO_NOPULL;
  // GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  // GPIO_InitStruct.Alternate = GPIO_AF1_TIM2;
  // HAL_GPIO_Init(STMOD_PWM_GPIO_Port, &GPIO_InitStruct);

  // /*Configure GPIO pin : SPI2_CS_Pin */
  // // GPIO_InitStruct.Pin = SPI2_CS_Pin;
  // // GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  // // GPIO_InitStruct.Pull = GPIO_NOPULL;
  // // GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  // // HAL_GPIO_Init(SPI2_CS_GPIO_Port, &GPIO_InitStruct);

  // /*Configure GPIO pin : ARD_D5_Pin */
  // GPIO_InitStruct.Pin = ARD_D5_Pin;
  // GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  // GPIO_InitStruct.Pull = GPIO_NOPULL;
  // GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  // GPIO_InitStruct.Alternate = GPIO_AF2_TIM5;
  // HAL_GPIO_Init(ARD_D5_GPIO_Port, &GPIO_InitStruct);

  // /*Configure GPIO pin : DSI_TE_Pin */
  // GPIO_InitStruct.Pin = DSI_TE_Pin;
  // GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  // GPIO_InitStruct.Pull = GPIO_NOPULL;
  // GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  // GPIO_InitStruct.Alternate = GPIO_AF11_DSI;
  // HAL_GPIO_Init(DSI_TE_GPIO_Port, &GPIO_InitStruct);

  // /*Configure GPIO pin : DCMI_HSYNC_Pin */
  // GPIO_InitStruct.Pin = DCMI_HSYNC_Pin;
  // GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  // GPIO_InitStruct.Pull = GPIO_NOPULL;
  // GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  // GPIO_InitStruct.Alternate = GPIO_AF10_DCMI;
  // HAL_GPIO_Init(DCMI_HSYNC_GPIO_Port, &GPIO_InitStruct);

  // /*Configure GPIO pin : ARD_A3_Pin */
  // GPIO_InitStruct.Pin = ARD_A3_Pin;
  // GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  // GPIO_InitStruct.Pull = GPIO_NOPULL;
  // GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  // HAL_GPIO_Init(ARD_A3_GPIO_Port, &GPIO_InitStruct);


  // /*Configure GPIO pin : MFX_WAKEUP_Pin */
  // GPIO_InitStruct.Pin = MFX_WAKEUP_Pin;
  // GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  // GPIO_InitStruct.Pull = GPIO_NOPULL;
  // GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  // HAL_GPIO_Init(MFX_WAKEUP_GPIO_Port, &GPIO_InitStruct);

  // /*Configure GPIO pin : DFDATIN1_Pin */
  // GPIO_InitStruct.Pin = DFDATIN1_Pin;
  // GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  // GPIO_InitStruct.Pull = GPIO_NOPULL;
  // GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  // GPIO_InitStruct.Alternate = GPIO_AF6_DFSDM1;
  // HAL_GPIO_Init(DFDATIN1_GPIO_Port, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
