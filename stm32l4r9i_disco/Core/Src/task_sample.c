/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "los_config.h"
#include "los_debug.h"
#include "los_interrupt.h"
#include "los_task.h"
#include "los_tick.h"

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif /* __cpluscplus */
#endif /* __cpluscplus */

#include "uart_if.h"
static DevHandle uart2;

UINT8 __attribute__ ((aligned (8))) g_memStart[OS_SYS_MEM_SIZE];

VOID TaskSampleEntry2(VOID)
{
    while (1) {
        LOS_TaskDelay(10000); /* 10 Seconds */
        printf("TaskSampleEntry2 running...\n");
    }
}

VOID TaskSampleEntry1(VOID)
{
    uart2 = UartOpen(2);
    if (uart2 == NULL) {
        printf("open uart2 fail\n");
    }
    while (1) {
        LOS_TaskDelay(2000); /* 2 Seconds */
        printf("TaskSampleEntry1 running...\n");
        UartWrite(uart2, "Write uart2 from task1 ...\n", 27);
    }
}

VOID TaskSample(VOID)
{
    UINT32 uwRet;
    UINT32 taskID1;
    UINT32 taskID2;
    TSK_INIT_PARAM_S stTask = {0};

    stTask.pfnTaskEntry = (TSK_ENTRY_FUNC)TaskSampleEntry1;
    stTask.uwStackSize = 0x1000;
    stTask.pcName = "TaskSampleEntry1";
    stTask.usTaskPrio = 6; /* Os task priority is 6 */
    uwRet = LOS_TaskCreate(&taskID1, &stTask);
    if (uwRet != LOS_OK) {
        printf("Task1 create failed\n");
    }

    stTask.pfnTaskEntry = (TSK_ENTRY_FUNC)TaskSampleEntry2;
    stTask.uwStackSize = 0x1000;
    stTask.pcName = "TaskSampleEntry2";
    stTask.usTaskPrio = 7; /* Os task priority is 7 */
    uwRet = LOS_TaskCreate(&taskID2, &stTask);
    if (uwRet != LOS_OK) {
        printf("Task2 create failed\n");
    }
}

VOID RunTaskSample(VOID)
{
    /* Kernel initialization */
    if (LOS_KernelInit() != LOS_OK) {
        return;
    }
    /* HDF ENTRY START */
    if (DeviceManagerStart() != 0) {
        printf("HDF START FAIL!\n");
    } else {
        printf("HDF START OK!\n");
    }

    TaskSample();
    LOS_Start();
}

#ifdef __cplusplus
#if __cplusplus
}
#endif /* __cpluscplus */
#endif /* __cpluscplus */
