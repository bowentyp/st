#include "../../Drivers/STM32L4xx_HAL_Driver/Inc/stm32l4xx_hal.h" 
#include "../../Drivers/STM32L4xx_HAL_Driver/Inc/stm32l4xx_hal_spi.h"
#include <stdint.h>

#define W25Q64 	0XEF16

extern uint16_t SPI_FLASH_TYPE;


#define FLASH_ID 0XEF14

#define W25X_WriteEnable		0x06 
#define W25X_WriteDisable		0x04 
#define W25X_ReadStatusReg		0x05 
#define W25X_WriteStatusReg		0x01 
#define W25X_ReadData			0x03 
#define W25X_FastReadData		0x0B 
#define W25X_FastReadDual		0x3B 
#define W25X_PageProgram		0x02 
#define W25X_BlockErase			0xD8 
#define W25X_SectorErase		0x20 
#define W25X_ChipErase			0xC7 
#define W25X_PowerDown			0xB9 
#define W25X_ReleasePowerDown	0xAB 
#define W25X_DeviceID			0xAB 
#define W25X_ManufactDeviceID	0x90 
#define W25X_JedecDeviceID		0x9F 

void SPI_Flash_Init(void);
uint16_t SPI_Flash_ReadID(void);
void SPI_Flash_Read(uint8_t*  ,uint32_t  ,uint16_t  );
void SPI_GPIO_Init(void);
void  HDF_Init_Test(void);